import {reduce} from "../src/async_lib/reduce";
import {delay, random} from "../src/async_lib/utils";
import { expect } from "chai";

const promise1 = Promise.resolve(3);
const promise2 = Promise.resolve(42);
const promise3 = new Promise((resolve, reject) => {
    setTimeout(resolve, 100, 4);
});


describe("Promise reduce tests", () => {
    // Applies only to tests in this describe block
    it("it is a function", () => {
      expect(reduce).to.be.a("function");
      expect(reduce).to.be.instanceOf(Function);
    });

    it("my reduce", async () => {
        const x= await reduce([ promise1, promise2, promise3 ],async (total,curr) => total+curr,0)
        expect(x).to.be.equals(49);
      });
  });


