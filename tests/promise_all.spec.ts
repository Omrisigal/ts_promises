import * as P from "../src/async_lib/promise_all";
import { expect } from "chai";



const promise1 = Promise.resolve(3);
const promise2 = 42;
const promise3 = new Promise((resolve, reject) => {
    setTimeout(resolve, 100, "foo");
});


describe("Promise All tests", () => {
    // Applies only to tests in this describe block
    it("it is a function", () => {
      expect(P.myAll).to.be.a("function");
      expect(P.myAll).to.be.instanceOf(Function);
    });

    it("simple sum", async () => {
        const x= await P.myAll([ promise1, promise2, promise3 ])
        expect(x).to.be.eql([3,42,"foo"]);
      });
   
   
  });



