import {some} from "../src/async_lib/race";
import { expect } from "chai";
import { delay } from "../src/async_lib/utils";

describe("Promise some tests", () => {
    // Applies only to tests in this describe block
    it("it is a function", () => {
      expect(some).to.be.a("function");
      expect(some).to.be.instanceOf(Function);
    });
  
      it("get the first X items that comeback, order1", async () => {
        const delayMsg = async (str: string, ms: number) => {
            await delay(ms);
            return str;
        };
        const promise0 = new Promise((resolve)=>{
            setTimeout(resolve, 1000, "0");
        });
        const promise1 = delayMsg("1", 2000);
        const promise2 = delayMsg("2", 100);
        const promise3 = delayMsg("3", 3000);
        let actual = await some([promise0, promise1,promise2,promise3],2);
        expect(actual).to.eql(["2","0"]);
    });


    it("get the first X items that comeback, order2", async () => {
        const delayMsg = async (str: string, ms: number) => {
            await delay(ms);
            return str;
        };
        const promise0 = new Promise((resolve)=>{
            setTimeout(resolve, 100, "0");
        });
        const promise1 = delayMsg("1", 2000);
        const promise2 = delayMsg("2", 1000);
        const promise3 = delayMsg("3", 3000);
        let actual = await some([promise0, promise1,promise2,promise3],2);
        expect(actual).to.eql(["0","2"]);
    });

      
  });



