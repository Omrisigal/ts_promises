import {myEach} from "../src/async_lib/each";
import {delay, random} from "../src/async_lib/utils";
import { expect } from "chai";


const promise1 = Promise.resolve(3);
const promise2 = Promise.resolve(42);
const promise3 = new Promise((resolve, reject) => {
    setTimeout(resolve, 100, "foo");
});

  describe(`Promise each2 tests`, () => {
    it(`should be a function`, () => {
        expect(myEach).to.be.a("function");
        expect(myEach).to.be.instanceOf(Function);
    });
    it(`test the promise each method`, async() => {
        let run = async () => {
            const result: string[] = [];
            await myEach("Omri", async char => {
                const c = (await char) as string;
                await delay(random(100,500));
                result.push(c.toUpperCase());
            })
            return result;
        };
        expect(await run()).to.eql(["O","M","R","I"]);
    });
});


