import * as P from "../src/async_lib/props";
import { expect } from "chai";

const promise1 = Promise.resolve(3);
const promise2 = 42;
const promise3 = new Promise((resolve, reject) => {
    setTimeout(resolve, 100, "foo");
});
let obj = {promise1, promise2, promise3}; 
let objresponse = {
    "promise1":3,
    "promise2":42,
    "promise3":"foo"
}

describe("Promise props tests", () => {
    // Applies only to tests in this describe block
    it("it is a function", () => {
      expect(P.myProps).to.be.a("function");
      expect(P.myProps).to.be.instanceOf(Function);
    });

    it("myProps", async () => {
        const x= await P.myProps(obj);
        expect(x).to.be.eql(objresponse);
      });
   
   
  });



