import * as P from "../src/async_lib/map";
import {delay,random} from "../src/async_lib/utils";
import { expect } from "chai";

const promise1 = Promise.resolve(3);
const promise2 = Promise.resolve(33);
const promise3 = Promise.resolve(333);


describe("Promise mapParallel tests", () => {
    // Applies only to tests in this describe block
    it("it is a function", () => {
      expect(P.mapParallel).to.be.a("function");
      expect(P.mapParallel).to.be.instanceOf(Function);
    });

    it("mapParallel", async () => {
        const x= await P.mapParallel([ promise1, promise2, promise3 ],async (item) => {
            await delay( random(100,100) );
            return await item+1; // Modify each item in the iterable
        })
        expect(x).to.be.eql([4,34,334]);
        });
   
  });
  
describe("Promise mapSeries tests", () => {
// Applies only to tests in this describe block
it("it is a function", () => {
    expect(P.mapSeries).to.be.a("function");
    expect(P.mapSeries).to.be.instanceOf(Function);
});

it("mapSeries", async () => {
    const x= await P.mapSeries([ promise1, promise2, promise3 ],async (item) => {
        await delay( random(100,100) );
        return await item+1; // Modify each item in the iterable
    })
    expect(x).to.be.eql([4,34,334]);
    });

});


