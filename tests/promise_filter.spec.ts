import * as P from "../src/async_lib/filter";
import {delay, random} from "../src/async_lib/utils";
import { expect } from "chai";


const promise1 = Promise.resolve(3);
const promise2 = Promise.resolve(33);
const promise3 = Promise.resolve(333);


describe("Promise filterParallel tests", () => {
    // Applies only to tests in this describe block
    it("it is a function", () => {
      expect(P.filterParallel).to.be.a("function");
      expect(P.filterParallel).to.be.instanceOf(Function);
    });

    it("filterParallel", async () => {
        const x = await P.filterParallel([ promise1, promise2, promise3 ], async (item) => {
            await delay( random(600,300) );
            const y = await item;
            return y>35; // Modify each item in the iterable
        })
        expect( x).to.be.eql([333]);
    });

    it("filterSeries", async () => {
        const x = await P.filterSeries([ promise1, promise2, promise3 ], async (item) => {
            await delay( random(600,300) );
            const y = await item;
            return y>35; // Modify each item in the iterable
        })
        expect( x).to.be.eql([333]);
    });
   
  });

