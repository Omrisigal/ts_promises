import log from "@ajar/marker";
import promise from "bluebird";

type all_input = any | Promise<any>;

export async function myAll(iterable: all_input[]): Promise<any[]> {
    // take an iterable
    // `all` returns a promise.
    const arr: any[] = [];
    for (const prom of iterable) {
        arr.push(await prom);
    }
    return arr;
}

//test

// function createPromise(time : number){
//     return new Promise((resolve,reject)=>{
//         setTimeout(()=>{resolve((Math.random()*100).toFixed(2));
//          },time);
//     });
// }

// const promise1 = Promise.resolve(3);
// const promise2 = 42;
// const promise3 = new Promise((resolve, reject) => {
// setTimeout(resolve, 100, "foo");
// });

//   //original All
// Promise.all([promise1, promise2, promise3]).then((values) => {
//   console.log(values);
// });

//   //myAll
//   myAll([promise1, promise2, promise3]).then((values) => {
//     console.log(values);
//   });
//   type all_input = any | Promise<any>;
