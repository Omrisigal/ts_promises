//--------------------------------------------------
type all_input = any | Promise<any> | Iterable<any>;
//type all_input = Iterable<any>;


export async function filterSeries(iterable: all_input[] | string, cb: (x: any) => boolean | Promise<boolean>) {
    const results = [];
    for (const item of iterable) {
        if ((await cb(item)) === true) results.push(await item);
    }
    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------

export async function filterParallel(iterable: all_input[] | string, cb: (x: any) => boolean | Promise<boolean>) {
    const results = [];
    const pending = Array.from(iterable, (item) => cb(item));

    for (const [i, p] of pending.entries()) {
        if ((await p) === true) results.push(await iterable[i]);
    }
    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------
