interface promiseObject {
    [key: string]: any;
}

export async function myProps(promisesObj: promiseObject) {
    const results = {} as promiseObject;
    for (const key in promisesObj) {
        results[key] = await promisesObj[key];
    }
    return results;
}
