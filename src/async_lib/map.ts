//--------------------------------------------------

type all_input = any | Promise<any> ;

export async function mapParallel(iterable: all_input[], cb: (x: any) => any) {
    const results = [];
    const pending = Array.from(iterable, (item) => cb(item));
    for (const p of pending) {
        results.push(await p);
    }
    return results;
}

//--------------------------------------------------

export async function mapSeries(iterable: all_input[] | string, cb: (x: any) => any) {
    const results = [];
    for (const item of iterable) {
        results.push(await cb(item));
    }
    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------
