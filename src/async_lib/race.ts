export async function some(promises : Promise<any>[],num = 1 ) {
    const results  = [] as Promise<any>[];
    return await new Promise((resolve)=>{
        promises.forEach(async p => {
            results.push(await p);
            if(results.length === num) resolve(results); 
        });
    });
}

export async function race(promises: Promise<any>[]) {
    return await new Promise((resolve)=>{
        promises.forEach(async p => {
            resolve(await p);
        });
    });
}