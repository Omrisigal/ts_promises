type all_input = any | Promise<any>;

export async function reduce(
    iterable: all_input,
    cb: (t: all_input, c: any) => any,
    initial: all_input = {}
) {
    let aggregator: all_input;

    if (initial) {
        aggregator = await initial;
    } else {
        aggregator = await iterable[0];
        iterable.shift();
    }
    for (const item of iterable) {
        aggregator = await cb(aggregator, await item);
    }
    return aggregator;
}
