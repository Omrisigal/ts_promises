type all_input = any | Promise<any>;

export async function myEach(
    iterable: all_input[] | string, cb: (x: any) => any): Promise<any[]> {
    // take an array and a function
    const res: any[] = [];
    for (let item of iterable) {
        if (item instanceof Promise) {
            item = await item;
        }
        item = await cb(item);
        res.push(item);
    }
    return res;
}

export async function each(
    iterable: Promise<any>[] | string | any |any[],
    cb: (
        iterable: Promise<any>,
        index?: number,
        iterableLength?: number
    ) => Promise<any>
): Promise<any[]> {
    for (const item of iterable) {
        await cb(item);
    }
    return iterable;
}

// export async function myEach(iterable: all_input[], cb : (x:any)=>any): Promise<any[]>{
//     for (const item of iterable) {
//         await cb(item);
//     }
//     return iterable;
// }