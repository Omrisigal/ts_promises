import log from "@ajar/marker";
import * as P from "./async_lib/index.js";

//myAll
////<------
// function createPromise(time : number){
//     return new Promise((resolve,reject)=>{
//         setTimeout(()=>{resolve((Math.random()*100).toFixed(2));
//          },time);
//     });
// }
const promise1 = Promise.resolve(3);
const promise2 = 42;
const promise3 = new Promise((resolve, reject) => {
setTimeout(resolve, 100, "foo");
});
// P.myAll([promise1, promise2, promise3]).then((values) => {
// console.log(values);
// });
// type all_input = any | Promise<any>;
    // P.all([ promise1, echoB('yo',3500), promise3 ])
    //     .then(values=> console.log('success ->',values))
    //     .catch(err=>log.red(err.message));

    // try{
    //     const resolved = await P.all([ promise1, echoB('yo',3500), promise3 ]);
    //     console.log(resolved) 
    // }catch(err){
    //     log.red(err.message)
    // }
// ////------>

// //my each<----
// const promise1 = Promise.resolve(3);
// const promise2 = 42;
// const promise3 = new Promise((resolve, reject) => {
//   setTimeout(()=>resolve(101), 100);
// });
// P.myEach([promise1, promise2, promise3],console.log).then((values) => {
//     console.log(values);
//   });
/**   P.each()  **/
    // log.yellow(

    //     await P.myEach("Geronimo", async char => {
    //         log.cyan(`${char} -->`);
    //         await P.delay( P.random(2000,500) );
    //         log.magenta(`<-- ${char}`);
    //         return char.toUpperCase(); // No effect..
    //     }) 
    // )
// //------>


// /**   P.filterParallel()  **/
   
        //filter only alphabetic characters
        // log.yellow(
        // await P.filterParallel("G<4!e3ro0ni1mo", async char => {
        //     log.cyan(`${char} -->`);
        //     await P.delay( P.random(2000,500) );
        //     log.magenta(`<-- ${char}`);
        //     return /^[A-Za-z]+$/.test(char); // test for alphabetic characters
        // }) 
        // )

    

// /**   P.filterSeries()  **/
    log.yellow(
        // filter only alphabetic characters
        await P.filterSeries([..."G<4!e3ro0ni1mo"], async char => {
            log.cyan(`${char} -->`);
            await P.delay( P.random(1000,100) );
            log.magenta(`<-- ${char}`);
            return /^[A-Za-z]+$/.test(char); // test for alphabetic characters
        }) 
    )

     // console.log(await P.myProps({ promise1, promise2, promise3 }));

    //  log.yellow(

    //     await P.mapParallel([promise1, promise2, promise3] , async p => {
    //         let char = await p;
    //         log.cyan(`${char} <--`);
    //         await P.delay( P.random(2000,500) );
    //         log.magenta(`<-- ${char}`);
    //         return char.toString().toUpperCase(); // Modify each item in the iterable
    //     }) 

    // ) 
    

/**   P.mapSeries()  **/
    // log.yellow(

    //     await P.mapSeries("Geronimo", async char => {
    //         log.cyan(`${char} -->`);
    //         await P.delay( P.random(2000,500) );
    //         log.magenta(`<-- ${char}`);
    //         return char.toUpperCase(); // Modify each item in the iterable
    //     }) 

    // )

    //   log.yellow(
    //     // filter only alphabetic characters
    //     await P.reduce([51,64,25,12,93], async (total,num) => {
    //         log.cyan(`${num} -->`);
    //         await P.delay( P.random(1000,100) );
    //         log.magenta(`<-- ${num}`);
    //         return total + num;
    //     },0) 

    // )

    /**   P.race()  **/
    // log.yellow(
    //     // filter only alphabetic characters
    //     await P.race([
    //             P.echo('first',4000),
    //             P.echo('second',1000),
    //             P.echo('third',3000)
    //         ]) 

    // )

/**   P.some()  **/
    // log.yellow(
    //     // filter only alphabetic characters
    //     await P.some([
    //             P.echo('first',4000),
    //             P.echo('second',400),
    //             P.echo('third',3000),
    //             P.echo('forth',3000),
    //             P.echo('fifth',500),
    //         ],2) 
    // )



