declare type all_input = any | Promise<any>;
export declare function myEach(iterable: all_input[] | string, cb: (x: any) => any): Promise<any[]>;
export {};
