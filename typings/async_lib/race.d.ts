export declare function some(promises: Promise<any>[], num?: number): Promise<unknown>;
export declare function race(promises: Promise<any>[]): Promise<unknown>;
