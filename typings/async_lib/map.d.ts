declare type all_input = any | Promise<any>;
export declare function mapParallel(iterable: all_input[], cb: (x: any) => any): Promise<any[]>;
export declare function mapSeries(iterable: all_input[] | string, cb: (x: any) => any): Promise<string | any[]>;
export {};
