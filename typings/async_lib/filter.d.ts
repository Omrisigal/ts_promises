declare type all_input = any | Promise<any> | Iterable<any>;
export declare function filterSeries(iterable: all_input[] | string, cb: (x: any) => any): Promise<string | any[]>;
export declare function filterParallel(iterable: all_input[] | string, cb: (x: any) => any): Promise<string | any[]>;
export {};
