interface promiseObject {
    [key: string]: any;
}
export declare function myProps(promisesObj: promiseObject): Promise<promiseObject>;
export {};
