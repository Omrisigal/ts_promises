declare type all_input = any | Promise<any>;
export declare function myAll(iterable: all_input[]): Promise<any[]>;
export {};
