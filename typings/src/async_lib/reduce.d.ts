declare type all_input = any | Promise<any>;
export declare function reduce(iterable: all_input, cb: (t: all_input, c: any) => any, initial?: all_input): Promise<any>;
export {};
